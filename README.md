# gatsby-starter-default-intl

A website for TEDxUniversityofMacedonia containing general information about the organization. Built with Gatsby.js

## License

MIT &copy; [Stathis Dimitriadis](https://gitlab.com/stathisdim)