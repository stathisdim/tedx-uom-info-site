import React from 'react'
import { Link } from '../i18n'
import Language from './language'

import { elastic as Menu } from 'react-burger-menu'

const ListLink = props => (
  <li
    style={{
      display: `inline`,
      marginRight: `1rem`,
      color: 'white',
      textDecoration: 'none',
    }}
  >
    <Link to={props.to} style={{ color: 'white' }} className="nav-link">
      {props.children}
    </Link>
  </li>
)

const Header = ({ siteTitle }) => (
  <div
    style={{
      background: '#070707',
      width: '100%',
      height: '3.7rem',
    }}
  >
    <div
      style={{
        margin: '0',
        padding: '0',
        display: 'flex',
        // height: '2rem',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
      }}
    >
      <h3 style={{ margin: 0, padding: '1rem 1.5rem' }}>
        <Link
          to="/"
          style={{
            color: 'white',
            textDecoration: 'none',
            onHover: {
              color: '#000000',
            },
          }}
        >
          {siteTitle}
        </Link>
      </h3>
      <nav>
        <Menu right pageWrapId={'page-wrap'} outerContainerId={'___gatsby'}>
          <ListLink to="/" className="menu-item">
            About
          </ListLink>
          <ListLink to="/talks/" className="menu-item">
            Talks
          </ListLink>
          <ListLink to="/events/" className="menu-item">
            Events
          </ListLink>
          <Language />
        </Menu>
      </nav>
    </div>
  </div>
)

export default Header
