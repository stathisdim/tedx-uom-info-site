import React from 'react'
import { OutboundLink } from 'gatsby-plugin-google-analytics'

import FbIcon from '../assets/fb_icon.svg'
import InstagramIcon from '../assets/instagram_icon.png'
import TwitterIcon from '../assets/twitter_icon.png'

const SocialIcon = ({ icon, index }) => (
  <li key={index} style={{padding: '5px'}}>
    <img
      src={icon}
      style={{
        width: '24px',
        height: '24px',
      }}
    />
  </li>
)

const SocialMediaIcons = () => {
  const iconsArray = [FbIcon, InstagramIcon, TwitterIcon]
  const IconElements = iconsArray.map((icon, index) => (
    <SocialIcon icon={icon} index={index} />
  ))

  console.log(IconElements)
  return (
    <ul
      style={{
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        listStyle: 'none'
      }}
    >
      {IconElements}
    </ul>
  )
}

const Footer = () => (
  <div
    style={{
      backgroundColor: '#070707',
      width: '100%',
      padding: '2rem 1.5rem',
      fontSize: '80%',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      margin: 0,
      position: 'absolute',
      height: '15rem',
      bottom: '0x',
      left: '0',
      right: '0',
    }}
  >
    <div className="footer-about">
      © TEDxUniversityofMacedonia<br />
      This Independent TEDx Event Is Operated Under License From TED<br />
      <OutboundLink
        href="mailto:info.tedxuom@gmail.com"
        style={{ color: '#e62b1e' }}
      >
        info.tedxuom@gmail.com
      </OutboundLink>
    </div>
    <div>
      <ul
        style={{
          listStyle: 'none',
          display: 'flex',
          flexDirection: 'column',
          fontSize: '120%',
        }}
      >
        <li style={{ display: 'block' }}>
          <OutboundLink href="https://blog.tedxuniversityofmacedonia.com">
            Blog
          </OutboundLink>
        </li>
        <li>
          <OutboundLink href="https://www.tedxuniversityofmacedonia.com">
            Current Event
          </OutboundLink>
        </li>
      </ul>
      <SocialMediaIcons />
    </div>
    <div />
  </div>
)

export default Footer
