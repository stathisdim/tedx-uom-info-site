import React from 'react'
import Helmet from 'react-helmet'
import { injectIntl } from 'react-intl'
import Header from './header'
import Footer from './footer'

import favicon from "../assets/favicon.ico"
import site_screen from "../assets/site_screen.png"

const Layout = ({ children, data, intl }) => (
  <div
    style={{
      width: '100vw',
      height: '100vh',
      maxWidth: '100%',
    }}
    id="outer-container"
  >
    <Helmet
      title={intl.formatMessage({ id: 'title' })}
      meta={[
        { name: 'description', content: intl.formatMessage({ id: 'welcome' }) },
        {
          name: 'keywords',
          content: 'gatsby, i18n, react-intl, multi language, localization',
        },
        {
          property: "og:image",
          content:  site_screen
        },
    
      ]}
    >
      <link rel="shortcut icon" type="image/png" href={favicon} sizes="16x16" />
    </Helmet>
    <Header siteTitle={intl.formatMessage({ id: 'title' })} />
    <div
      id="page-wrap"
      style={{
        margin: '2rem auto',
        maxWidth: '1200px',
        padding: ' 0 1.0875rem 10rem 1.0875rem',
        flex: 1,
      }}
    >
      {children}
    </div>
    <Footer />
  </div>
)

export default injectIntl(Layout)
